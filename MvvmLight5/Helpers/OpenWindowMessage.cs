﻿namespace MvvmLight5.Helpers {
  public class OpenWindowMessage {
    public WindowType Type { get; set; }
    public string Argument { get; set; }
  }
}